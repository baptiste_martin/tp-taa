# TAA TP Service REST et Persistance JPA / GLI TP Angular JS #
Baptiste MARTIN    
Fernando DIAZ ANGELES ORTEGA

Backend WebService Rest et Persistance JPA.


**Installation et exécution**

Il est nécessaire de paramétrer l'accès à une base de données qui contiendra une table avec les informations nécessaires pour l'authentification, pour cela il est nécessaire de modifier le fichier de configuration de la persistence : */src/main/resources/META-INF/persistence.xml*

La configuration fournie utilise un server local de base de données MySQL sur le port par défaut *3306* avec une base nommée : *covoiturage*. L'utilisateur *root* avec un mot de passe vide est utilisé pour l'authentification.

Il est possible de construire le fichier WAR avec Maven et de le déployer sur l'instance Tomcat en cours d'exécution avec le goal *redeploy* du plugin Tomcat de Maven comme décrit ci-dessous dans la section External Tomcat Deployment using Maven.

Le dossier taa */ src / main / webapp /* contient un client écrit en AngularJS. Il faut exécuter la commande *bower update* dans le répertoire */ src / main / webapp / js * et sélectionner la version angular#1.3.14. Une fois le fichier WAR déployé l'application client est disponible sur : [http://localhost:8080/carpooling](http://localhost:8080/carpooling)

**How to debug in mvn tomcat plugin**

Execute :

```
#!bash

export MAVEN_OPTS=-agentlib:jdwp=transport=dt_socket,address=8000,server=y,suspend=n &&
mvn tomcat7:run
```


If in external tomcat :
open :
/etc/init.d/tomcat7
and change :

```
#!bash

catalina_sh start $SECURITY
```
 (line 216)
to :

```
#!bash

set JPDA_ADDRESS=8000
set JPDA_TRANSPORT=dt_socket
catalina_sh jpda start $SECURITY
```


And in Eclipse debug in Remote Application.

**External Tomcat Deployment using Maven** 

The following command allows you to deploy this application as a war to a currently running instance of Tomcat 7, you must run it from project root folder (taa)

```
#!bash

mvn tomcat7:redeploy 
```
Using the -DskipTests option to skip testing minimizes launch time.

Make sure to add an "admin" user to tomcat configuration by inserting the following two lines to conf/tomcat-users.xml :


```
#!xml

<role rolename="manager-script"/>
<user username="admin" password="" roles="manager-script"/>
```


**Références**

Persistance :
http://www.java2s.com/Code/JavaAPI/javax.persistence/EntityTransactionbegin.htm

Hibernet: 
https://www.mistra.fr/tutoriel-hibernate-creation-manipulation-entites-jpa/tutoriel-hibernate-creation-entite-jpa.html

Objet d'accès aux données :
https://www.mistra.fr/tutoriel-jee-application-web-pgejeev1/tutoriel-jee-pgejeev1-dao.html