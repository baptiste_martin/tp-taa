/**
 * Module for carpooling project
 */
var carpoolingApp = angular.module('CarpoolingApp', ['ngDialog', 'ngRoute', 'ui.bootstrap', 'ListTextBox']);

/**
 * Create filter to enum list of int
 */
carpoolingApp.filter('range', function() {
	return function(input, min, max) {
		min = parseInt(min); //Make string input int
		max = parseInt(max);
		for (var i=min; i<max; i++)
			input.push(i);
		return input;
	};
});

carpoolingApp.config(function($routeProvider, $locationProvider) {
	$routeProvider
		.when(carpooling.pages.login, {
			templateUrl: 'templates/login.html',
			controller: 'CnxController'
		})
		.when(carpooling.pages.signUp, {
			templateUrl: 'templates/signup.html',
			controller: 'CnxController'
		})
		.when(carpooling.pages.user, {
			templateUrl: 'templates/user.html',
			controller: 'UserController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.userUpdate, {
			templateUrl: 'templates/user.html',
			controller: 'UserController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.car, {
			templateUrl: 'templates/car.html',
			controller: 'CarController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.carAdd, {
			templateUrl: 'templates/car.html',
			controller: 'CarController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.carUpdate, {
			templateUrl: 'templates/car.html',
			controller: 'CarController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.trip, {
			templateUrl: 'templates/trip.html',
			controller: 'TripController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.tripAdd, {
			templateUrl: 'templates/trip.html',
			controller: 'TripController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.tripUpdate, {
			templateUrl: 'templates/trip.html',
			controller: 'TripController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.comment, {
			templateUrl: 'templates/comment.html',
			controller: 'CommentController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.commentAdd, {
			templateUrl: 'templates/comment.html',
			controller: 'CommentController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.when(carpooling.pages.commentUpdate, {
			templateUrl: 'templates/comment.html',
			controller: 'CommentController',
			resolve : {
				factory : function ($location) {
					carpooling.checkConnection($location);
				}
			}
		})
		.otherwise({
			redirectTo: carpooling.pages.login
		});
	
	// configure html5 to get links
	$locationProvider.html5Mode(true);
});

/**
 * Create shared data alerts
 */
carpoolingApp.factory('Alerts', function($timeout) {
	var alerts = new Array();
	
	return {
		get: function() {
			return alerts;
		},
		
		push: function(alert) {
			alerts.push(alert);
			$timeout(function() {
				alerts.splice(alerts.length - 1, 1);
			}, 10000);
		},
		
		splice: function(index, nb) {
			alerts.splice(index, nb);
		}
	};
});

carpoolingApp.controller('HeaderController', function($scope, $location, Alerts) {
	"use strict";	// set interpretation mode on strict
	$scope.alerts = Alerts.get();
	
	/**
	 * Check if user is authenticate for show navigation menu
	 */
	$(document).on('showMenu', function() {
		$scope.showNav = $.cookie("username") == undefined;
	});
	
	$scope.tabActive = function(name) {
		switch (name) {
			case "user" : return $location.path().replace('/carpooling', '').match('/user');
			case "car" : return $location.path().replace('/carpooling', '').match('/car');
			case "trip" : return $location.path().replace('/carpooling', '').match('/trip');
			case "comment" : return $location.path().replace('/carpooling', '').match('/comment');
			default : return false;
		}
	};
	
	/**
	 * Disconnect the user
	 */
	$scope.disconnect = function() {
		$.removeCookie("username");
		$(document).trigger('showMenu');
		$location.path(carpooling.pages.login);
	};
	
	/**
	 * Close alert for index
	 */
	$scope.closeAlert = function(index) {
		Alerts.splice(index, 1);
	};

	$(document).trigger('showMenu'); // initialization
});

/**
 * Controller for connection management
 */
carpoolingApp.controller('CnxController', function($scope, $http, $location, Alerts) {
	"use strict";	// set interpretation mode on strict
	
	/**
	 * Post the authentication
	 */
	$scope.postLogin = function() {
		if ($scope.login != undefined || $scope.password != undefined) {
			var encryptPass = carpooling.encryptPass($scope.password);
			var data = { username: $scope.login, password: encryptPass };
			$http(carpooling.post(carpooling.webscript.post_login, data))
				.success(function(data, status, headers, config) {
					if (data != null && data == true) {
						$.cookie('username', $scope.login, { path: '/' });
						$(document).trigger('showMenu');
						$location.path(carpooling.pages.user);
						Alerts.push({ msg: 'Authentification success', type: 'success' });
					} else {
						Alerts.push({ msg: 'Authentification error' });
					}
				})
				.error(function(data, status, headers, config) {
					Alerts.push({ msg: 'Error ' + status, type: 'danger' });
				});
		} else {
			Alerts.push({ msg: 'Login or password enpty' });
		}
	};
	
	/**
	 * Change path
	 */
	$scope.signUp = function() {
		$location.path(carpooling.pages.signUp);
	}
	
	/**
	 * Put the new user
	 */
	$scope.putRegister = function() {
		if ($scope.login != undefined || $scope.password != undefined || $scope.mail != undefined || 
				$scope.phone != undefined) {
			var encryptPass = carpooling.encryptPass($scope.password);
			var data =	{
							username: $scope.login,
							password: encryptPass,
							mail: $scope.mail,
							phone: $scope.phone
						};
			$http(carpooling.put(carpooling.webscript.put_user, data))
				.success(function(data, status, headers, config) {
					if (data != null) {
						$.cookie('username', $scope.login, { path: '/' });
						$(document).trigger('showMenu');
						$location.path(carpooling.pages.user);
						Alerts.push({ msg: 'Registering success', type: 'success' });
					} else {
						Alerts.push({ msg: 'Registering error' });
					}
				})
				.error(function(data, status, headers, config) {
					Alerts.push({ msg: 'Error ' + status, type: 'danger' });
				});
		} else {
			Alerts.push({ msg: 'All fill of form is required' });
		}
	};
	
	/**
	 * Pretty printer for phone number
	 */
	$scope.printPhone = function() {
		$scope.phone = carpooling.printPhone($scope.phone);
	};
});