carpoolingApp.controller('UserController', function($scope, $http, $route, $location, ngDialog, Alerts) {
	"use strict";	// set interpretation mode on strict
	
	if ($location.path().match("/update$")) {
		// Update user profile
		$("#info-user").hide();
		$("#update-user").show();
	} else {
		// Show user profile
		$("#update-user").hide();
		$("#info-user").show();
	}
	
	/**
	 * Get user information
	 */
	$scope.getPerson = function() {
		$http(carpooling.get(carpooling.webscript.get_user.replace(':username', $.cookie("username"))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.user = data;
					$scope.isCollapsed = true;
					
					if ($location.path().match("/update$")) {
						$scope._injectData(data);
					}
				} else {
					Alerts.push({ msg: 'Get user error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Send comment for user
	 */
	$scope.getCommentsForUser = function() {
		$http(carpooling.get(carpooling.webscript.get_comment_with_username
				.replace(':username', $.cookie('username'))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.userComments = data;
				} else {
					Alerts.push({ msg: 'Get comments error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Inject data to form
	 */
	$scope._injectData = function(user) {
		$scope.oldPassword = user.password;
		$scope.mail = user.mail;
		$scope.confirmMail = user.mail;
		$scope.phone = user.phone;
	};
	
	/**
	 * Inject data to form and redirect
	 */
	$scope.updateUser = function() {
		$http(carpooling.get(carpooling.webscript.get_user.replace(':username', $.cookie("username"))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope._injectData(data);
					$location.path(carpooling.pages.userUpdate);
				} else {
					Alerts.push({ msg: 'Get user error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	}
	
	/**
	 * Redirect to update comment
	 */
	$scope.updateComment = function(id, tripId) {
		$location.path(carpooling.pages.commentUpdate.replace(":tripId", tripId).replace(":id", id));
	}
		
	/**
	 * Post update user information
	 */
	$scope.postUser = function() {
		var encryptPass = ($scope.password != undefined) ? carpooling.encryptPass($scope.password) : '';
		var data =	{
						username: $.cookie("username"),
						password: (encryptPass != '') ? encryptPass : $scope.oldPassword,
						reEncodePassword: (encryptPass != '') ? true : false,
						mail: $scope.mail,
						phone: $scope.phone
					};
		$http(carpooling.post(carpooling.webscript.post_user, data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.user = data;
					$location.path(carpooling.pages.user);
					Alerts.push({ msg: 'Update user success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Update user error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Popin delete trip
	 */
	$scope.popinDeleteComment = function(id, comment) {
		carpooling.showPopin(ngDialog, "Are you sure to delete comment :<br />" + 
				comment + " ?", "deleteComment(" + id + ")", $scope);
	};

	/**
	 * Send delete trip with id
	 */
	$scope.deleteComment = function(id) {
		$http(carpooling.delete(carpooling.webscript.delete_comment
				.replace(':username', $.cookie("username"))
				.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getCommentsForUser();
					Alerts.push({ msg: 'Delete comment success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Detete comment error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Pretty printer for phone number
	 */
	$scope.printPhone = function() {
		if ($scope.phone != undefined) {
			$scope.phone = carpooling.printPhone($scope.phone);
		}
	};
	
});