carpoolingApp.controller('TripController', function($scope, $http, ngDialog, $route, $location, $filter, Alerts) {
	"use strict";	// set interpretation mode on strict
	
	// Check what article show
	if ($location.path().match("/add$")) {
		// Add trip
		$("#list-trip").hide();
		$("#update-trip").hide();
		$("#btnUpdateTrip").hide();
		
		$("#add-update-trip").show();
		$("#add-trip").show();
		$("#btnAddTrip").show();
	} else if ($location.path().match("/update/([0-9]*)$") && $route.current.params.id  != undefined) {
		// Update trip
		$("#list-trip").hide();
		$("#add-trip").hide();
		$("#btnAddTrip").hide();
		
		$("#update-trip").show();
		$("#add-update-trip").show();
		$("#btnUpdateTrip").show();
	} else {
		// List trip
		$("#add-update-trip").hide();
		
		$("#list-trip").show();
	}

	/**
	 * Send get all trip
	 */
	$scope.getTrips = function() {
		$http(carpooling.get(carpooling.webscript.get_trips))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.trips = data;
					$scope.username = $.cookie('username');
					
					if ($location.path().match("/update/([0-9]*)$") && $route.current.params.id != undefined) {
						for (var i = 0; i < data.length; i++) {
							var trip = data[i];
							if (trip.id == $route.current.params.id) {
								$scope._injectData(trip);
							}
						}
					}
				} else {
					Alerts.push({ msg: 'Get trips error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Send search trip with start, end, (dateMin and dateMax not mandatory)
	 */
	$scope.searchTrips = function() {
		var query = "";
		
		if ($scope.dateMin != undefined && $scope.dateMin != null) {
			query += "?dateMin=" + $scope.dateMin.getTime();
			if ($scope.dateMax != undefined && $scope.dateMax != null) {
				query += "&dateMax=" + $scope.dateMax.getTime();
			}
		} else if ($scope.dateMax != undefined && $scope.dateMax != null) {
			query += "?dateMax=" + $scope.dateMax.getTime();
		}
		
		$http(carpooling.get(carpooling.webscript.get_search_trip
				.replace(':start', $scope.start)
				.replace(':end', $scope.end) + query))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.trips = data;
				} else {
					Alerts.push({ msg: 'Search trips error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	}
	
	/**
	 * Get car to user
	 */
	$scope.getCar = function() {
		$http(carpooling.get(carpooling.webscript.get_cars.replace(':username', $.cookie("username"))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.cars = data;
				} else {
					Alerts.push({ msg: 'Get car error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Inject data to form
	 */
	$scope._injectData = function(trip) {
		$scope.id = trip.id;
		$scope.car = { id: trip.car.id, label: trip.car.model };
		$scope.start = trip.start;
		$scope.stops = trip.stops;
		$scope.end = trip.end;
		$scope.date = trip.startDate;
		$scope.nbOfFreeSeats = trip.nbOfFreeSeats;
	}
	
	/**
	 * Get car of car list
	 */
	$scope._getCar = function() {
		var car = null;
		for (var i = 0; i < $scope.cars.length; i++) {
			if ($scope.cars[i].id == $scope.car.id) {
				car = $scope.cars[i];
				break;
			}
		}
		return car;
	}

	/**
	 * Get trip of trip list
	 */
	$scope._getTrip = function(id) {
		var trip = null;
		for (var i = 0; i < $scope.trips.length; i++) {
			if ($scope.trips[i].id == id) {
				trip = $scope.trips[i];
				break;
			}
		}
		return trip;
	}
	
	/**
	 * Clear data to form and redirect
	 */
	$scope.addTrip = function() {
		$scope._injectData(
				{id: "", car: {id: 0}, start: "", stops: [], end: "", startDate: null, nbOfFreeSeats: 0});
		$location.path(carpooling.pages.tripAdd);
	};

	/**
	 * Send add trip
	 */
	$scope.putTrip = function() {
		var data =	{
						car: $scope._getCar(),
						start: $scope.start,
						stops: $scope.stops,
						end: $scope.end,
						startDate: $scope.date,
						nbOfFreeSeats: $scope.nbOfFreeSeats
					};
		$http(carpooling.put(carpooling.webscript.put_trip.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.trip);
					Alerts.push({ msg: 'Add trip success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Add trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Inject data to form and redirect
	 */
	$scope.updateTrip = function(id) {
		$http(carpooling.get(carpooling.webscript.get_trip.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope._injectData(data);
					$location.path(carpooling.pages.tripUpdate.replace(":id", data.id));
				} else {
					Alerts.push({ msg: 'Get trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Send update trip
	 */
	$scope.postTrip = function() {
		var data =	{
						id: $scope.id,
						car: $scope._getCar(),
						start: $scope.start,
						stops: $scope.stops,
						end: $scope.end,
						startDate: $scope.date,
						nbOfFreeSeats: $scope.nbOfFreeSeats
					};
		$http(carpooling.post(carpooling.webscript.post_trip.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.trip);
					Alerts.push({ msg: 'Update trip success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Update trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Popin delete trip
	 */
	$scope.popinDeleteTrip = function(id, start, end, startDate) {
		carpooling.showPopin(ngDialog, "Are you sure to delete trip going from " + start + 
				" to " + end + " on " + $filter('date')(startDate, 'dd-MMMM-yyyy') + "?", "deleteTrip(" + id + ")", $scope);
	};

	/**
	 * Send delete trip with id
	 */
	$scope.deleteTrip = function(id) {
		$http(carpooling.delete(carpooling.webscript.delete_trip
				.replace(':username', $.cookie("username"))
				.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getTrips();
					Alerts.push({ msg: 'Delete trip success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Detete trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Popin join trip
	 */
	$scope.popinJoinTrip = function(id, start, end, startDate) {
		carpooling.showPopin(ngDialog, "Are you sure to join trip going from " + start + 
				" to " + end + " on " + $filter('date')(startDate, 'dd-MMMM-yyyy') + "?", "joinTrip(" + id + ")", $scope);
	};

	/**
	 * Send join trip with id
	 */
	$scope.joinTrip = function(id) {
		var data = $scope._getTrip(id);
		$http(carpooling.post(carpooling.webscript.join_trip.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getTrips();
					Alerts.push({ msg: 'Join trip success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Join trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Popin unjoin trip
	 */
	$scope.popinUnjoinTrip = function(id, start, end, startDate) {
		carpooling.showPopin(ngDialog, "Are you sure to leave trip going from " + start + 
				" to " + end + " on " + $filter('date')(startDate, 'dd-MMMM-yyyy') + "?", "unjoinTrip(" + id + ")", $scope);
	};

	/**
	 * Send unjoin trip with id
	 */
	$scope.unjoinTrip = function(id) {
		var data = $scope._getTrip(id);
		$http(carpooling.post(carpooling.webscript.unjoin_trip.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getTrips();
					Alerts.push({ msg: 'Unjoin trip success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Unjoin trip error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Test if user is passenger of the trip id
	 */
	$scope.isPassenger = function(id) {
		var trip = $scope._getTrip(id);
		for (var i = 0; i < trip.passengers.length; i++) {
			if (trip.passengers[i].username == $.cookie("username")) {
				return true;
			}
		}
		return false;
	};
	
	/**
	 * Open calendar
	 */
	$scope.open = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.opened = true;
	};

	/**
	 * Open calendar
	 */
	$scope.openMin = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedMin = true;
	};

	/**
	 * Open calendar
	 */
	$scope.openMax = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedMax = true;
	};
});