/**
 * URL to server
 */
const _urlRest = "http://localhost:8080/rest";

/**
 * Singleton Util class for application
 */
var carpooling = {
	/**
	 * Pages to this application
	 */
	pages : {
		login : "/carpooling/login",
		signUp : "/carpooling/signup",
		user : "/carpooling/user",
		userUpdate : "/carpooling/user/update",
		car : "/carpooling/car",
		carAdd : "/carpooling/car/add",
		carUpdate : "/carpooling/car/update/:id",
		trip : "/carpooling/trip",
		tripAdd : "/carpooling/trip/add",
		tripUpdate : "/carpooling/trip/update/:id",
		comment: "/carpooling/comment",
		commentAdd: "/carpooling/comment/add/:tripId",
		commentUpdate: "/carpooling/comment/update/:tripId/:id"
	},
	
	/**
	 * Webscript to call REST service
	 */
	webscript : {
		// Login URL
		post_login : _urlRest + "/person/login",

		// User URLs
		get_user : _urlRest + "/person/:username",
		put_user : _urlRest + "/person",
		post_user : _urlRest + "/person",

		// Car URLs
		get_cars : _urlRest + "/car/driver/:username",
		get_car : _urlRest + "/car/:id",
		put_car : _urlRest + "/car",
		post_car : _urlRest + "/car",
		delete_car : _urlRest + "/car/:id",

		// Trip URLs
		get_trips : _urlRest + "/trip/all",
		get_trips_with_username : _urlRest + "/trip/with/:username",
		get_trip : _urlRest + "/trip/:id",
		get_search_trip : _urlRest + "/trip/search/:start/:end",
		get_search_trip_with_username : _urlRest + "/trip/search/:username/:start/:end",
		put_trip : _urlRest + "/trip/:username",
		post_trip : _urlRest + "/trip/:username",
		join_trip : _urlRest + "/trip/join/:username",
		unjoin_trip : _urlRest + "/trip/unjoin/:username",
		delete_trip : _urlRest + "/trip/:username/:id",

		// Comment URLs
		get_comment : _urlRest + "/comment/:id",
		get_comment_with_trip : _urlRest + "/comment/withTrip/:id",
		get_comment_with_username : _urlRest + "/comment/withPerson/:username",
		put_comment : _urlRest + "/comment/:username",
		post_comment : _urlRest + "/comment/:username",
		delete_comment : _urlRest + "/comment/:username/:id"
	},
	
	/**
	 * Structure for POST method in AngularJS
	 */
	post : function(service, data) {
		"use strict";	// set interpretation mode on strict
		
		return {method: 'POST', url: service, data: data};
	},
	
	/**
	 * Structure for PUT method in AngularJS
	 */
	put : function(service, data) {
		"use strict";	// set interpretation mode on strict
		
		return {method: 'PUT', url: service, data: data};
	},
	
	/**
	 * Structure for GET method in AngularJS
	 */
	get : function(service) {
		"use strict";	// set interpretation mode on strict
		
		return {method: 'GET', url: service};
	},

	/**
	 * Structure for DELETE method in AngularJS
	 */
	delete : function(service) {
		"use strict";	// set interpretation mode on strict
		
		return {method: 'DELETE', url: service};
	},
	
	_toByte : function(str) {
		var buf = new Uint16Array(str.length);
		
		for (var i = 0; i < str.length; i++) {
			buf[i] = str.charCodeAt(i);
		}
		
		return buf;
	},
	
	_getMin : function(byteArray) {
		var min = byteArray[0];
		
		for (var i = 1; i < byteArray.length; i++) {
			if (byteArray[i] < min) {
				min = byteArray[i];
			}
		}
		
		return min;
	},

	/**
	 * Encrypt password for transfer to service
	 */
	encryptPass: function(password) {
		"use strict";	// set interpretation mode on strict

		var passArray = this._toByte(password);
		var encodedPass = new Uint16Array(passArray.length * 4 + 1);
		var mask = (Math.floor(Math.random() * this._getMin(passArray)) - 2) + 1;
		
		encodedPass[0] = mask;
		
		var j = 1;
		for (var i = 0; i < passArray.length; i++) {
			encodedPass[j++] = passArray[i] - mask;
			encodedPass[j++] = passArray[i] - mask;
			encodedPass[j++] = passArray[i] - mask;
			encodedPass[j++] = passArray[i] - mask;
		}
		
		return String.fromCharCode.apply(String, encodedPass);
	},
	
	_currentPage: undefined,
	
	/**
	 * Check if user is connected
	 */
	checkConnection: function(location) {
		if ($.cookie("username") == undefined) {
			this._currentPage = location.path();
			location.path(this.pages.login);
		} else {
			if (this._currentPage != undefined) {
				location.path(this._currentPage);
				this._currentPage = undefined;
			}
		}
		$(document).trigger('showMenu');
	},
	
	showPopin: function(ngDialog, msg, func, scope) {
		ngDialog.openConfirm({
			template: 
				"<div>\
				<p>" + msg + "</p>\
				<button ng-click=\"" + func + ";closeThisDialog();\">Yes</button>\
				<button ng-click=\"closeThisDialog();\">No</button>\
				</div>",
			className: 'ngdialog-theme-flat ngdialog-theme-custom',
			scope: scope,
			plain: true
		});
	},
	
	printPhone: function(phone) {
		phone = phone.replace(/ /g, '');
		
		return phone.substr(0, 2) + ' ' +
			((phone.length > 3) ? phone.substr(2, 2) + ' ' : '') +
			((phone.length > 5) ? phone.substr(4, 2) + ' ' : '') +
			((phone.length > 7) ? phone.substr(6, 2) + ' ' : '') +
			((phone.length > 9) ? phone.substr(8, 2) : '');
	}
}