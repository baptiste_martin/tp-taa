/**
 * Module for ui list text box
 */
angular.module('ListTextBox', [])

.controller('ListTextBoxController', function ($scope) {
	$scope.array = new Array({ id: 0, value: "" });
	$scope.ngModelCtrl;
	
	$scope.add = function() {
		$scope.array.push({ id: $scope.array.length, value: "" });
	};
	
	$scope.remove = function(item) {
		var array = new Array(), id = 0, value = new Array();
		
		for (var i = 0; i < $scope.array.length; i++) {
			if (item != i) {
				value.push($scope.array[i].value);
				array.push({ id: id, value: $scope.array[i].value });
				id++;
			}
		}

		$scope.ngModelCtrl.$setViewValue(value);
		$scope.array = array;
	};
})

.directive('listTextBox', function() {
	return {
		controller: 'ListTextBoxController',
		templateUrl: 'templates/ui/listTextBox.html',
		terminal: true,
		require: "?ngModel",
		replace: true,
		link: function(scope, element, attrs, ngModelCtrl) {
			if (!ngModelCtrl) return;
			
			scope.ngModelCtrl = ngModelCtrl;
			
			// Listen for change events to enable binding
			element.on('blur keyup change', function() {
				scope.$evalAsync(read);
			});
			read(); // initialize
			
			function read() {
				var array = new Array();
				
				for (var i = 0; i < scope.array.length; i++) {
					var value = $("#listTextBox-element" + i).val();
					scope.array[i].value = value;
					if (value != null && value != "") array.push(value);
				}
				
				ngModelCtrl.$setViewValue(array);
			};
			
			// set function to render injected value
			ngModelCtrl.$render = function() {
				for (var i = 0; i < ngModelCtrl.$viewValue.length; i++) {
					var value = ngModelCtrl.$viewValue[i];
					
					if (scope.array[i])
						scope.array[i] = { id: i, value: value };
					else
						scope.array.push({ id: i, value: value });
				}
			};
		}
	}
});