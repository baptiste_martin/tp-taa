carpoolingApp.controller('CommentController', function($scope, $http, ngDialog, $route, $location, Alerts) {
	"use strict";	// set interpretation mode on strict
	
	/**
	 * Inject data to form
	 */
	$scope._injectData = function(comment) {
		$scope.id = comment.id;
		$scope.tripId = comment.trip.id;
		$scope.comment = comment.comments;
		$scope.rating = comment.rating;
	}

	// Check what article show
	if ($location.path().match("/add/([0-9]*)$") && $route.current.params.tripId != undefined) {
		// Add comment
		$("#list-trip").hide();
		$("#update-comment").hide();
		$("#btnUpdateComment").hide();
		
		$("#add-update-comment").show();
		$("#add-comment").show();
		$("#btnAddComment").show();
		
		$scope._injectData({id: "", comments: "", rating: 1, trip: {id: $route.current.params.tripId} });
	} else if ($location.path().match("/update/([0-9]*)/([0-9]*)$") && $route.current.params.tripId != undefined
			&& $route.current.params.id != undefined) {
		// Update comment
		$("#list-trip").hide();
		$("#add-comment").hide();
		$("#btnAddComment").hide();
		
		$("#update-comment").show();
		$("#add-update-comment").show();
		$("#btnUpdateComment").show();
	} else {
		// List trip
		$("#add-update-comment").hide();
		
		$("#list-trip").show();
	}

	/**
	 * Check if current user are already comment trip for id
	 */
	$scope.isCommented = function(tripId) {
		for (var i = 0; i < $scope.trips.length; i++) {
			if ($scope.trips[i].id == tripId) {
				var trip = $scope.trips[i];
				for (var j = 0; trip.comments != null && j < trip.comments.length; j++) {
					if (trip.comments[j].person.username == $.cookie("username")) {
						return true;
					}
				}
				return false;
			}
		}
		return false;
	};

	/**
	 * Send get all trip
	 */
	$scope.getTrips = function() {
		$http(carpooling.get(carpooling.webscript.get_trips_with_username
				.replace(':username', $.cookie('username'))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.trips = data;
					$scope.username = $.cookie('username');
					
					for (var i = 0; i < $scope.trips.length; i++) {
						var trip = $scope.trips[i];
						trip.isCollapsed = true;
						$http(carpooling.get(carpooling.webscript.get_comment_with_trip
								.replace(':id', trip.id)))
							.success(function(data, status, headers, config) {
								if (status == 200) {
									trip.comments = data;
									
									if ($location.path().match("/update/([0-9]*)/([0-9]*)$") && 
											$route.current.params.tripId != undefined && $route.current.params.id != undefined) {
										for (var i = 0; i < data.length; i++) {
											var comment = data[i];
											if (comment.id == $route.current.params.id) {
												comment.trip = { id: trip.id };
												$scope._injectData(comment);
											}
										}
									}
								} else {
									Alerts.push({ msg: 'Get comment error (status: ' + status + ')' });
								}
							})
							.error(function(data, status, headers, config) {
								Alerts.push({ msg: 'Error ' + status, type: 'danger' });
							});
					}
				} else {
					Alerts.push({ msg: 'Get trips error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Send search trip with start, end, (dateMin and dateMax not mandatory)
	 */
	$scope.searchTrips = function() {
		var query = "";
		
		if ($scope.dateMin != undefined && $scope.dateMin != null) {
			query += "?dateMin=" + $scope.dateMin.getTime();
			if ($scope.dateMax != undefined && $scope.dateMax != null) {
				query += "&dateMax=" + $scope.dateMax.getTime();
			}
		} else if ($scope.dateMax != undefined && $scope.dateMax != null) {
			query += "?dateMax=" + $scope.dateMax.getTime();
		}
		
		$http(carpooling.get(carpooling.webscript.get_search_trip_with_username
				.replace(':username', $.cookie('username'))
				.replace(':start', $scope.start)
				.replace(':end', $scope.end) + query))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.trips = data;

					for (var i = 0; i < $scope.trips.length; i++) {
						$scope.trips[i].isCollapsed = true;
					}
				} else {
					Alerts.push({ msg: 'Search trips error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	}
	
	
	/**
	 * Get trip with current id
	 */
	$scope._getTrip = function() {
		var trip = null;
		for (var i = 0; i < $scope.trips.length; i++) {
			if ($scope.trips[i].id == $scope.tripId) {
				trip = $scope.trips[i];
				delete trip["isCollapsed"];
				break;
			}
		}
		return trip;
	}
	
	/**
	 * Clear data to form and redirect
	 */
	$scope.addComment = function(tripId) {
		$location.path(carpooling.pages.commentAdd.replace(':tripId', tripId));
	};

	/**
	 * Send add comment
	 */
	$scope.putComment = function() {
		var data =	{
						trip: $scope._getTrip(),
						comments: $scope.comment,
						rating: $scope.rating
					};
		$http(carpooling.put(carpooling.webscript.put_comment.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.comment);
					Alerts.push({ msg: 'Add comment success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Add comment error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Inject data to form and redirect
	 */
	$scope.updateComment = function(id, tripId) {
		$http(carpooling.get(carpooling.webscript.get_comment.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					data.trip = { id: tripId };
					$scope._injectData(data);
					$location.path(carpooling.pages.commentUpdate.replace(":tripId", tripId).replace(":id", data.id));
				} else {
					Alerts.push({ msg: 'Get comment error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Send update comment
	 */
	$scope.postComment = function() {
		var data =	{
						id: $scope.id,
						trip: $scope._getTrip(),
						rating: $scope.rating,
						comments: $scope.comment,
					};
		$http(carpooling.post(carpooling.webscript.post_comment.replace(':username', $.cookie("username")), data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.comment);
					Alerts.push({ msg: 'Update comment success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Update comment error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Popin delete trip
	 */
	$scope.popinDeleteComment = function(id, comment) {
		carpooling.showPopin(ngDialog, "Are you sure to delete comment :<br />" + 
				comment + " ?", "deleteComment(" + id + ")", $scope);
	};

	/**
	 * Send delete trip with id
	 */
	$scope.deleteComment = function(id) {
		$http(carpooling.delete(carpooling.webscript.delete_comment
				.replace(':username', $.cookie("username"))
				.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getTrips();
					Alerts.push({ msg: 'Delete comment success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Detete comment error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Open calendar
	 */
	$scope.openMin = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedMin = true;
	};

	/**
	 * Open calendar
	 */
	$scope.openMax = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.openedMax = true;
	};
});