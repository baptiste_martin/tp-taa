carpoolingApp.controller('CarController', function($scope, $http, ngDialog, $route, $location, Alerts) {
	"use strict";	// set interpretation mode on strict

	// Check what article show
	if ($location.path().match("/add$")) {
		// Add car
		$("#list-car").hide();
		$("#update-car").hide();
		$("#btnUpdateCar").hide();
		
		$("#add-update-car").show();
		$("#add-car").show();
		$("#btnAddCar").show();
	} else if ($location.path().match("/update/([0-9]*)$") && $route.current.params.id  != undefined) {
		// Update car
		$("#list-car").hide();
		$("#add-car").hide();
		$("#btnAddCar").hide();
		
		$("#update-car").show();
		$("#btnUpdateCar").show();
		$("#add-update-car").show();
	} else {
		// List user car
		$("#add-update-car").hide();
		
		$("#list-car").show();
	}
	
	/**
	 * Get car to user
	 */
	$scope.getCar = function() {
		$http(carpooling.get(carpooling.webscript.get_cars.replace(':username', $.cookie("username"))))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope.cars = data;
					
					if ($location.path().match("/update/([0-9]*)$") && $route.current.params.id != undefined) {
						for (var i = 0; i < data.length; i++) {
							var car = data[i];
							if (car.id == $route.current.params.id) {
								$scope._injectData(car);
							}
						}
					}
				} else {
					Alerts.push({ msg: 'Get cars error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};

	/**
	 * Inject data to form
	 */
	$scope._injectData = function(car) {
		$scope.id = car.id;
		$scope.model = car.model;
		$scope.nbOfSeats = car.nbOfSeats;
		$scope.hasAC = car.hasAC;
		$scope.isSmoker = car.isSmoker;
		$scope.hasRadio = car.hasRadio;
	}
	
	/**
	 * Clear data to form and redirect
	 */
	$scope.addCar = function() {
		$scope._injectData(
				{id: "", model: "", bOfSeats: 0, hasAC: false, isSmoker: false, hasRadio: false});
		$location.path(carpooling.pages.carAdd);
	};

	/**
	 * Send add car
	 */
	$scope.putCar = function() {
		var data =	{
						driver:	{
							username:$.cookie("username")
						},
						model: $scope.model,
						nbOfSeats: $scope.nbOfSeats,
						hasAC: $scope.hasAC,
						isSmoker: $scope.isSmoker,
						hasRadio: $scope.hasRadio
					};
		$http(carpooling.put(carpooling.webscript.put_car, data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.car);
					Alerts.push({ msg: 'Add car success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Add car error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Inject data to form and redirect
	 */
	$scope.updateCar = function(id) {
		$http(carpooling.get(carpooling.webscript.get_car.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$scope._injectData(data);
					$location.path(carpooling.pages.carUpdate.replace(":id", data.id));
				} else {
					Alerts.push({ msg: 'Get car error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Send update car
	 */
	$scope.postCar = function() {
		var data =	{
				id: $scope.id,
				driver:	{
					username:$.cookie("username")
				},
				model: $scope.model,
				nbOfSeats: $scope.nbOfSeats,
				hasAC: $scope.hasAC,
				isSmoker: $scope.isSmoker,
				hasRadio: $scope.hasRadio
			};
		$http(carpooling.post(carpooling.webscript.post_car, data))
			.success(function(data, status, headers, config) {
				if (status == 200) {
					$location.path(carpooling.pages.car);
					Alerts.push({ msg: 'Update car success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Update car error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
	
	/**
	 * Popin delete car
	 */
	$scope.popinDeleteCar = function(id, model) {
		carpooling.showPopin(ngDialog, "Are your sure to delete " + model + " car", "deleteCar(" + id + ")", 
				$scope);
	};
	
	/**
	 * Send delete car with id
	 */
	$scope.deleteCar = function(id) {
		$http(carpooling.delete(carpooling.webscript.delete_car.replace(':id', id)))
			.success(function(data, status, headers, config) {
				if (status == 204) {
					$scope.getCar();
					Alerts.push({ msg: 'Delete car success', type: 'success' });
				} else {
					Alerts.push({ msg: 'Delete car error (status: ' + status + ')' });
				}
			})
			.error(function(data, status, headers, config) {
				Alerts.push({ msg: 'Error ' + status, type: 'danger' });
			});
	};
});