package fr.istic.m2.taa;

import java.util.LinkedHashSet;
import java.util.Set;

public class SetUtils {
	public static <U, E> Set<E> removeElementOfSet(E deleteElement, Set<E> set) {
		Set<E> result = new LinkedHashSet<E>();
		for (E e : set) {
			if (!e.equals(deleteElement)) {
				result.add(e);
			}
		}
		return result;
	}
}
