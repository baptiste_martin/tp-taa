package fr.istic.m2.taa.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.service.dao.DaoUtils;

@Path("/car")
public class CarService {
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCars(@PathParam("id") long id) {
		return DaoUtils.getInstance().getCarDao().get(id);
	}
		
	@GET
	@Path("/driver/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Car> getCars(@PathParam("username") String driverName) {
		Person driver = DaoUtils.getInstance().getPersonDao().get(driverName);
		return DaoUtils.getInstance().getCarDao().getByDriver(driver);
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Car addCar(Car car) {
		Person driver = DaoUtils.getInstance().getPersonDao().get(car.getDriver().getUsername());
		
		car.setDriver(driver);
		
		DaoUtils.getInstance().getCarDao().add(car);
		
		driver.setReEncodePassword(false);
		driver.getCars().add(car);
		
		DaoUtils.getInstance().getPersonDao().update(driver);
		
		return car;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Car updateCar(Car car) {
		Person driver = DaoUtils.getInstance().getPersonDao().get(car.getDriver().getUsername());
		Car oldCar = DaoUtils.getInstance().getCarDao().get(car.getId());
		
		oldCar.setModel(car.getModel());
		oldCar.setNbOfSeats(car.getNbOfSeats());
		oldCar.setDriver(driver);
		oldCar.setHasAC(car.getHasAC());
		oldCar.setIsSmoker(car.getIsSmoker());
		oldCar.setHasRadio(car.getHasRadio());
		
		return DaoUtils.getInstance().getCarDao().update(oldCar);
	}
	
	@DELETE
	@Path("/{id}")
	public void deleteCar(@PathParam("id") long id) {
		Car car = DaoUtils.getInstance().getCarDao().get(id);
		Person driver = DaoUtils.getInstance().getPersonDao().get(car.getDriver().getUsername());

		driver.getCars().remove(car);
		DaoUtils.getInstance().getPersonDao().update(driver);
		
		DaoUtils.getInstance().getCarDao().delete(car);
	}
}
