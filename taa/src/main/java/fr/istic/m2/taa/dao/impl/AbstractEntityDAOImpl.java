package fr.istic.m2.taa.dao.impl;

import javax.persistence.EntityManager;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.dao.EntityDAO;

public abstract class AbstractEntityDAOImpl<I, T> implements EntityDAO<I, T> {	
	public T add(T t) {
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		entityManager.persist(t);
		entityManager.getTransaction().commit();
		
		entityManager.close();

		return t;
	}

	public T update(T t) {
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		t = entityManager.merge(t);
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return t;
	}

	public void delete(T t) {
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		t = entityManager.merge(t);
		entityManager.remove(t);
		entityManager.getTransaction().commit();
		
		entityManager.close();
	}
}
