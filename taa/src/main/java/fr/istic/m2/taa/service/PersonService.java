package fr.istic.m2.taa.service;

import java.security.MessageDigest;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;

import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.service.crypto.PasswordEncoder;
import fr.istic.m2.taa.service.dao.DaoUtils;

@Path("/person")
public class PersonService {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> getPersons() {
		return DaoUtils.getInstance().getPersonDao().getAll();
	}
	
	@GET
	@Path("/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Person getPerson(@PathParam("username") String username) {
		return DaoUtils.getInstance().getPersonDao().get(username);
	}
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Person getPerson(Person person) {
		person.setPassword(PasswordEncoder.decodePassword(person.getPassword()));
		return DaoUtils.getInstance().getPersonDao().add(person);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Person setPerson(Person person) {
		Person oldPerson = DaoUtils.getInstance().getPersonDao().get(person.getUsername());
		if (person.getReEncodePassword()) {
			oldPerson.setPassword(PasswordEncoder.decodePassword(person.getPassword()));
		}
		oldPerson.setMail(person.getMail());
		oldPerson.setPhone(person.getPhone());
		return DaoUtils.getInstance().getPersonDao().update(oldPerson);
	}
	
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean login(Person p) {
		Person person = DaoUtils.getInstance().getPersonDao().get(p.getUsername());
		boolean connect = false;
		
		try {
			connect = person!= null && person.getPassword().equals(
				DatatypeConverter.printHexBinary( 
					MessageDigest
						.getInstance("MD5")
						.digest(PasswordEncoder.decodePassword(p.getPassword()).getBytes("UTF-8"))
				));
		} catch (Exception e) {}
		
		return connect;
	}
}
