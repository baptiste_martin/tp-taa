package fr.istic.m2.taa.dao.impl;

import java.security.MessageDigest;
import java.util.List;

import javax.persistence.EntityManager;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.dao.PersonDAO;
import fr.istic.m2.taa.model.Person;

@SuppressWarnings("unchecked")
public class PersonDAOImpl extends AbstractEntityDAOImpl<String, Person> implements PersonDAO {
	private static final Logger LOGGER = Logger.getLogger(PersonDAOImpl.class);

	public List<Person> getAll() {
		List<Person> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select p from Person p").getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public Person get(String username) {
		Person result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		try {
			result = (Person) entityManager.createQuery("select p from Person p where username = :username")
					.setParameter("username", username)
					.getSingleResult();
		} catch (Exception e){
			LOGGER.error(e.getMessage(), e);
		}
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public Person add(Person person) {	
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		if (!entityManager.contains(person)) {
			try {
				person.setPassword(DatatypeConverter.printHexBinary(
						MessageDigest.getInstance("MD5").digest(person.getPassword().getBytes("UTF-8"))));
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		} else {
			LOGGER.warn("Person " + person.getUsername() + " already exist.");
			person = null;
		}
		entityManager.getTransaction().commit();
		
		entityManager.close();

		return (person == null) ? null : super.update(person);
	}

	public Person update(Person person) {
		if (person.getReEncodePassword()) {
			try {
				person.setPassword(DatatypeConverter.printHexBinary(
						MessageDigest.getInstance("MD5").digest(person.getPassword().getBytes("UTF-8"))));
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}
		}
		
		return super.update(person);
	}
}
