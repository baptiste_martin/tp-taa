package fr.istic.m2.taa.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.codehaus.jackson.annotate.JsonIgnore;

@Entity
public class Car {	
	private long id;
	private String model;
	private int nbOfSeats;
	private Person driver;
	private boolean hasAC;
	private boolean isSmoker;
	private boolean hasRadio;
	private Set<Trip> trips;

	public Car() {
		this.model = "";
		this.nbOfSeats = 0;
		this.driver = null;
		this.hasAC = false;
		this.isSmoker = false;
		this.hasRadio = false;
		this.trips = new LinkedHashSet<>();
	}
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public Car setId(long id) {
		this.id = id;
		return this;
	}

	public String getModel() {
		return model;
	}
	public Car setModel(String model) {
		this.model = model;
		return this;
	}

	public int getNbOfSeats() {
		return nbOfSeats;
	}
	public Car setNbOfSeats(int nbOfSeats) {
		this.nbOfSeats = nbOfSeats;
		return this;
	}

	@ManyToOne(targetEntity=Person.class)
	public Person getDriver() {
		return driver;
	}
	public Car setDriver(Person driver) {
		this.driver = driver;
		return this;
	}
	
	@OneToMany(targetEntity=Trip.class, fetch = FetchType.EAGER, mappedBy="car", orphanRemoval = true)
	@JsonIgnore
	public Set<Trip> getTrips() {
		return trips;
	}
	public void setTrips(Set<Trip> trips) {
		this.trips = trips;
	}

	public boolean getHasAC() {
		return hasAC;
	}
	public Car setHasAC(boolean hasAC) {
		this.hasAC = hasAC;
		return this;
	}

	public boolean getIsSmoker() {
		return isSmoker;
	}
	public Car setIsSmoker(boolean isSmoker) {
		this.isSmoker = isSmoker;
		return this;
	}

	public boolean getHasRadio() {
		return hasRadio;
	}
	public Car setHasRadio(boolean hasRadio) {
		this.hasRadio = hasRadio;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Car)) {
			throw new ClassCastException();
		}
		Car castObj = (Car) obj;
		
		return castObj.id == this.id;
	}
	
	@Override
	public int hashCode(){
		return Long.valueOf(id).hashCode();
	}
}