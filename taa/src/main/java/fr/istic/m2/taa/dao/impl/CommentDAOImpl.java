package fr.istic.m2.taa.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.dao.CommentDAO;
import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

@SuppressWarnings("unchecked")
public class CommentDAOImpl extends AbstractEntityDAOImpl<Long, Comment> implements CommentDAO {
	public List<Comment> getAll() {
		List<Comment> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Comment c").getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public Comment get(Long id) {
		Comment result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = (Comment) entityManager.createQuery("select c from Comment c where id = :id")
				.setParameter("id", id)
				.getSingleResult();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public List<Comment> getByTrip(Trip trip) {
		List<Comment> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Comment c where trip = :trip")
				.setParameter("trip", trip)
				.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public List<Comment> getByRating(int rating) {
		List<Comment> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Comment c where rating = :rating")
				.setParameter("rating", rating)
				.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public List<Comment> getByPerson(Person person) {
		List<Comment> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Comment c where person = :person")
				.setParameter("person", person)
				.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}
}