package fr.istic.m2.taa.dao;

import java.util.List;

public interface EntityDAO<I, T> {
	public List<T> getAll();
	public T get(I o);
	public T add(T t);
	public T update(T t);
	public void delete(T t);
}
