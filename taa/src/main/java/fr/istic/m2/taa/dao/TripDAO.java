package fr.istic.m2.taa.dao;

import java.util.Date;
import java.util.List;

import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

public interface TripDAO extends EntityDAO<Long, Trip> {
	public void join(Person passenger, Trip trip);
	public void unjoin(Person passenger, Trip trip);
	public List<Trip> getByDateAndPlaces(String startCity, String endCity, Date dateMin, Date dateMax);
	public List<Trip> getByPerson(Person p);
	public List<Trip> getByPersonDateAndPlaces(Person p, String startCity, String endCity, Date dateMin, Date dateMax);
}
