package fr.istic.m2.taa.model;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;

@Entity
public class Person {	
	private String username;
	private String password;
	private boolean reEncodePassword;
	private String phone;
	private String mail;
	private Set<Car> cars;
	private Set<Comment> comments;
	private Set<Trip> trips;
	
	public Person() {
		this.username = "";
		this.password = "";
		this.phone = "";
		this.mail = "";
		this.cars = new LinkedHashSet<>();
		this.comments = new LinkedHashSet<>();
		this.trips = new LinkedHashSet<>();
	}

	@Id
	public String getUsername() {
		return username;
	}
	public Person setUsername(String username) {
		this.username = username;
		return this;
	}

	@JsonIgnore // for not serialize password
	public String getPassword() {
		return password;
	}
	@JsonProperty // for deserialize password
	public Person setPassword(String password) {
		this.password = password;
		return this;
	}
	
	@Transient // for not persist in data base
	@JsonIgnore // for not serialize password
	public boolean getReEncodePassword() {
		return reEncodePassword;
	}
	@JsonProperty // for deserialize password
	public Person setReEncodePassword(boolean reEncodePassword) {
		this.reEncodePassword = reEncodePassword;
		return this;
	}
	
	public String getPhone() {
		return phone;
	}
	public Person setPhone(String phone) {
		this.phone = phone;
		return this;
	}
	
	public String getMail() {
		return mail;
	}
	public Person setMail(String mail) {
		this.mail = mail;
		return this;
	}	

	@OneToMany(targetEntity=Car.class, fetch = FetchType.EAGER, mappedBy="driver", cascade={CascadeType.REMOVE}, orphanRemoval = true)
	@JsonIgnore
	public Set<Car> getCars() {
		return cars;
	}
	public Person setCars(Set<Car> cars) {
		this.cars = cars;
		return this;
	}

	@OneToMany(targetEntity=Comment.class, fetch = FetchType.EAGER, mappedBy="person", cascade={CascadeType.REMOVE}, orphanRemoval = true)
	@JsonIgnore
	public Set<Comment> getComments() {
		return comments;
	}
	public Person setComments(Set<Comment> comments) {
		this.comments = comments;
		return this;
	}
	
	@ManyToMany(targetEntity=Trip.class, fetch=FetchType.EAGER)
	@JsonIgnore
	public Set<Trip> getTrips() {
		return trips;
	}
	public Person setTrips(Set<Trip> trips) {
		this.trips = trips;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Person)) {
			throw new ClassCastException();
		}
		
		Person castObj = (Person) obj;
		return castObj.username.equals(this.username);
	}
	
	@Override
	public int hashCode(){
		return username.hashCode();
	}
}
