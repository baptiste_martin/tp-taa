package fr.istic.m2.taa.service.dao;

import fr.istic.m2.taa.dao.CarDAO;
import fr.istic.m2.taa.dao.CommentDAO;
import fr.istic.m2.taa.dao.PersonDAO;
import fr.istic.m2.taa.dao.TripDAO;
import fr.istic.m2.taa.dao.impl.CarDAOImpl;
import fr.istic.m2.taa.dao.impl.CommentDAOImpl;
import fr.istic.m2.taa.dao.impl.PersonDAOImpl;
import fr.istic.m2.taa.dao.impl.TripDAOImpl;

public enum DaoUtils {
	//private static final DaoUtils INSTANCE = new DaoUtils();
	INSTANCE;
	private CarDAO car_dao;
	private PersonDAO person_dao;
	private CommentDAO comment_dao;
	private TripDAO trip_dao;
	
	private DaoUtils() {
		car_dao = new CarDAOImpl();
		person_dao = new PersonDAOImpl();
		comment_dao = new CommentDAOImpl();
		trip_dao = new TripDAOImpl();
	}

	public static DaoUtils getInstance() {
		return INSTANCE;
	}
	
	public CarDAO getCarDao() {
		return car_dao;
	}
	
	public PersonDAO getPersonDao() {
		return person_dao;
	}
	
	public CommentDAO getCommentDao() {
		return comment_dao;
	}
	
	public TripDAO getTripDao() {
		return trip_dao;
	}
}
