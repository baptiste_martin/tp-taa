package fr.istic.m2.taa.dao;

import java.util.List;

import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;

public interface CarDAO extends EntityDAO<Long, Car> {
	public List<Car> getByDriver(Person driver);
}
