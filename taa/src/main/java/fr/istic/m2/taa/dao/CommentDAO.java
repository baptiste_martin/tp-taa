package fr.istic.m2.taa.dao;

import java.util.List;

import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

public interface CommentDAO extends EntityDAO<Long, Comment>{
	public List<Comment> getByPerson(Person person);
	public List<Comment> getByTrip(Trip trip);
	public List<Comment> getByRating(int rating);
}
