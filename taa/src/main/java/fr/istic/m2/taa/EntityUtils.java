package fr.istic.m2.taa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityUtils {
	private static EntityUtils INSTANCE = new EntityUtils();
	private static EntityManagerFactory entityManagerFactory;
	private static String currentMode = "prod";
	
	private EntityUtils() {
		entityManagerFactory = Persistence.createEntityManagerFactory("prod");
	}
	
	public EntityUtils getInstance() {
		return INSTANCE;
	}
	
	public static EntityManager getEntityManager() {
		return entityManagerFactory.createEntityManager();
	}
	
	public static void setMode(String mode) {
		if (!mode.equals("dev") && !mode.equals("test") && !mode.equals("prod")) {
			throw new IllegalArgumentException();
		} else if (!currentMode.equals(mode)) {
			entityManagerFactory = Persistence.createEntityManagerFactory(mode);
			currentMode = mode;
		}
	}
}
