package fr.istic.m2.taa.dao;

import fr.istic.m2.taa.model.Person;

public interface PersonDAO extends EntityDAO<String, Person> {
}
