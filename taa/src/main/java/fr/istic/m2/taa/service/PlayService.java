package fr.istic.m2.taa.service;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;
import fr.istic.m2.taa.service.dao.DaoUtils;

@Path("/play")
public class PlayService {
	
	/**
	 * REST Attend Service method to take over a current trip by changing the driver and number of free seats.
	 * @param username Current login name who will be the new trip driver.
	 * @param freeSeats number of free seats.
	 * @param trip json object of current trip.
	 * @return true if successfully attended the trip
	 */
	@POST
	@Path("/{username}/{freeseats}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean attend(@PathParam("username") String username, @PathParam("freeseats") int freeSeats, Trip trip) {
		boolean result = false;
		Person user = DaoUtils.getInstance().getPersonDao().get(username);
		if (user != null) {
			if (!trip.getCar().getDriver().equals(user)) {
				changeTripDriver(trip, user);
			}
			trip.setNbOfFreeSeats(freeSeats);
			DaoUtils.getInstance().getTripDao().update(trip);
			result = true;
		}
		return result;
	}
	
	/**
	 * REST Join service method to join current trip while changing its driver.
	 * @param username login used by user that joins the trip.
	 * @param driverName login used by user that is chosen as driver.
	 * @param trip json objet of current trip
	 * @return true if successfully joined the trip
	 */
	@POST
	@Path("join/{drivername}/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean join(@PathParam("username") String username, @PathParam("drivername") String driverName, Trip trip) {
		boolean result = false;
		Person newDriver = DaoUtils.getInstance().getPersonDao().get(driverName);
		if (newDriver != null){
			Person passenger = DaoUtils.getInstance().getPersonDao().get(username);
			if (passenger != null) {
				if (!trip.getCar().getDriver().equals(newDriver)) {
					changeTripDriver(trip, newDriver);
					DaoUtils.getInstance().getTripDao().update(trip);
				}
				DaoUtils.getInstance().getTripDao().join(passenger, trip);
				result = true;
			}
		}
		return result;
	}
	
	/**
	 * Change the given trip driver.
	 * It creates a dummy car to handle the trip if there's no car for the given user,
	 * it picks any car of the user's cars set otherwise.
	 * @param trip Trip to change.
	 * @param user new driver user name.
	 */
	public void changeTripDriver(Trip trip, Person user) {
		Set<Car> userCars = user.getCars();
		if (!userCars.contains(trip.getCar())){ //if the previous trip car is not a user's car
			if (userCars.isEmpty()){
				// Create a dummy car for this driver with n + 1 places to be used in this trip
				Car newCar = new Car()
					.setDriver(user)
					.setModel("playDummyCar")
					.setNbOfSeats(trip.getNbOfFreeSeats() + 1);
				DaoUtils.getInstance().getCarDao().add(newCar);
				trip.setCar(newCar);
			} else {
				// Pick any car for this trip from users cars set
				for(Car aUserCar: userCars) {
					trip.setCar(aUserCar);
				    break;
				}
			}
		}
	}
}