package fr.istic.m2.taa.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import fr.istic.m2.taa.EntityUtils;
//import fr.istic.m2.taa.SetUtils;
import fr.istic.m2.taa.dao.TripDAO;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

@SuppressWarnings("unchecked")
public class TripDAOImpl extends AbstractEntityDAOImpl<Long, Trip> implements TripDAO {
	public List<Trip> getAll() {
		List<Trip> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select t from Trip t where startDate >= :date")
				.setParameter("date", new Date(), TemporalType.DATE)
				.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public Trip get(Long id) {
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		Trip result = (Trip) entityManager.createQuery("select t from Trip t where t.id = :id")
				.setParameter("id", id)
				.getSingleResult();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}

	public void join(Person passenger, Trip trip) {
		if (passenger == null || trip == null){
			throw new IllegalArgumentException("Passenger and trip must be not null to join a trip");
		}
		
		if (trip.getCar().getDriver().getUsername().equals(passenger.getUsername())) {
			throw new IllegalAccessError("You're already the car's driver, you can't join this trip as a passenger.");
		}
		
		if (trip.getNbOfFreeSeats() == 0) {
			throw new IllegalAccessError("No more seats are available for this trip");
		}
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		
		if (!trip.getPassengers().contains(passenger)) {
			trip.getPassengers().add(passenger);
			trip.setNbOfFreeSeats(trip.getNbOfFreeSeats() - 1);
			passenger.getTrips().add(trip);
		}
		
		entityManager.merge(trip);
		entityManager.merge(passenger);
		entityManager.getTransaction().commit();
		
		entityManager.close();
	}

	public void unjoin(Person passenger, Trip trip) {
		if (passenger == null || trip == null){
			throw new IllegalArgumentException("Passenger and trip must be not null to unjoin a trip");
		}
		
		if (trip.getCar().getDriver().getUsername().equals(passenger.getUsername())) {
			throw new IllegalAccessError("You're already the car's driver, you can't unjoin this trip.");
		}
		
		EntityManager entityManager = EntityUtils.getEntityManager();

		entityManager.getTransaction().begin();
		
		if (trip.getPassengers().contains(passenger)) {
			trip.getPassengers().remove(passenger);
			trip.setNbOfFreeSeats(trip.getNbOfFreeSeats() + 1);
			passenger.getTrips().remove(trip);
		}
		
		entityManager.merge(trip);
		entityManager.merge(passenger);
		entityManager.getTransaction().commit();
		
		entityManager.close();
	}

	public List<Trip> getByDateAndPlaces(String start, String end, Date dateMin, Date dateMax) {
		List<Trip> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(
				"select distinct t from Trip t left join t.stops stops" +
				" where (startCity like :start or stops like :start)" +
				" and (stops like :end or endCity like :end)" +
				((dateMin != null) ? " and startDate >= :dateMin" : "") +
				((dateMax != null) ? " and startDate <= :dateMax" : ""))
				.setParameter("start", '%' + start + '%')
				.setParameter("end",  '%' + end + '%');
		
		if (dateMin != null) {
			query.setParameter("dateMin", dateMin, TemporalType.DATE);
		}
		
		if (dateMax != null) {
			query.setParameter("dateMax", dateMax, TemporalType.DATE);
		}

		result = query.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}
	
	public List<Trip> getByPerson(Person p) {
		List<Trip> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select t from Trip t left join t.passengers passengers"
				+ " where passengers = :passenger")
				.setParameter("passenger", p)
				.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}
	
	public List<Trip> getByPersonDateAndPlaces(Person p, String start, String end, Date dateMin,
			Date dateMax) {
		List<Trip> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(
				"select t from Trip t left join t.stops stops left join t.passengers passengers where passengers = :passenger" +
				" and (startCity like :start or stops like :start)" +
				" and (stops like :end or endCity like :end)" +
				((dateMin != null) ? " and startDate >= :dateMin" : "") +
				((dateMax != null) ? " and startDate <= :dateMax" : ""))
				.setParameter("passenger", p)
				.setParameter("start", '%' + start + '%')
				.setParameter("end",  '%' + end + '%');
		
		if (dateMin != null) {
			query.setParameter("dateMin", dateMin, TemporalType.DATE);
		}
		
		if (dateMax != null) {
			query.setParameter("dateMax", dateMax, TemporalType.DATE);
		}

		result = query.getResultList();
		entityManager.getTransaction().commit();
		
		entityManager.close();
		
		return result;
	}
}