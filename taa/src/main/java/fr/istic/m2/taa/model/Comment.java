package fr.istic.m2.taa.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.sun.istack.NotNull;

import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

@Entity
public class Comment {	
	private long id;
	private Person person;
	private Trip trip;
	private int rating;	
	private String comments;	

	public Comment() {
		this.person = null;
		this.trip = null;
		this.rating = 0;
		this.comments = "";
	}
	
	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public Comment setId(long id) {
		this.id = id;
		return this;
	}

	@ManyToOne(targetEntity=Person.class)
	@JoinColumn(name = "person_username")
	public Person getPerson() {
		return person;
	}
	public Comment setPerson(Person person) {
		this.person = person;
		return this;
	}
	
	@ManyToOne(targetEntity=Trip.class)
	@JoinColumn(name = "trip_id")
	public Trip getTrip() {
		return trip;
	}
	public Comment setTrip(Trip trip) {
		this.trip = trip;
		return this;
	}
	
	public int getRating() {
		return rating;
	}
	public Comment setRating(int rating) {
		this.rating = rating;
		return this;
	}
	
	@NotNull
	@Lob
	@Column(length=500)
	public String getComments() {
		return comments;
	}
	public Comment setComments(String comments) {
		this.comments = comments;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Comment)) {
			throw new ClassCastException();
		}
		Comment castObj = (Comment) obj;
		
		return castObj.id == this.id;
	}
	
	@Override
	public int hashCode(){
		return Long.valueOf(id).hashCode();
	}
}
