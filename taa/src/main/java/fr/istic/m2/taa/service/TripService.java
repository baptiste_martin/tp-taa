package fr.istic.m2.taa.service;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;
import fr.istic.m2.taa.service.dao.DaoUtils;

@Path("/trip")
public class TripService {
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Trip> getTrips() {
		return DaoUtils.getInstance().getTripDao().getAll();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Trip getTrips(@PathParam("id") Long id) {
		return DaoUtils.getInstance().getTripDao().get(id);
	}

	@GET
	@Path("/with/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Trip> getTripsWithUsername(@PathParam("username") String username) {
		Person p = DaoUtils.getInstance().getPersonDao().get(username);
		return DaoUtils.getInstance().getTripDao().getByPerson(p);
	}
	
	@GET
	@Path("/search/{startCity}/{endCity}")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Trip> search(@QueryParam("dateMin") long dateMin, 
			@QueryParam("dateMax") long dateMax, @PathParam("startCity") String startCity, 
			@PathParam("endCity") String endCity) {
		return DaoUtils.getInstance().getTripDao().getByDateAndPlaces(startCity, endCity,
				(dateMin > 0) ? new Date(dateMin) : null, (dateMax > 0) ? new Date(dateMax) : null);
	}

	@GET
	@Path("/search/{username}/{startCity}/{endCity}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Trip> searchWithUsername(@PathParam("username") String username, 
			@QueryParam("dateMin") long dateMin, @QueryParam("dateMax") long dateMax, 
			@PathParam("startCity") String startCity, @PathParam("endCity") String endCity) {
		Person p = DaoUtils.getInstance().getPersonDao().get(username);
		return DaoUtils.getInstance().getTripDao().getByPersonDateAndPlaces(p, startCity, endCity, 
				(dateMin > 0) ? new Date(dateMin) : null, (dateMax > 0) ? new Date(dateMax) : null);
	}
	
	@PUT
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Trip addTrip(@PathParam("username") String username, Trip trip) {
		if (!trip.getCar().getDriver().getUsername().equals(username)) {
			throw new IllegalAccessError("Only the car's driver is allowed to add a trip.");
		}
		trip = DaoUtils.getInstance().getTripDao().add(trip);

		Car car = trip.getCar();
		car.getTrips().add(trip);
				
		DaoUtils.getInstance().getCarDao().update(car);
		
		return trip;
	}
	
	@POST
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Trip setTrip(@PathParam("username") String username, Trip trip) {
		if (!trip.getCar().getDriver().getUsername().equals(username)) {
			throw new IllegalAccessError("Only the car's driver is allowed to modify a trip.");
		}
		//TODO Test if obligate
//		Trip oldTrip = DaoUtils.getInstance().getTripDao().get(trip.getId());
//		oldTrip.setStart(trip.getStart());
//		oldTrip.setStops(trip.getStops());
//		oldTrip.setEnd(trip.getEnd());
//		oldTrip.setNbOfFreeSeats(trip.getNbOfFreeSeats());
		
		return DaoUtils.getInstance().getTripDao().update(trip);
	}
	
	@POST
	@Path("join/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void joinTrip(@PathParam("username") String username, Trip trip) {
		Person passenger = DaoUtils.getInstance().getPersonDao().get(username);
		DaoUtils.getInstance().getTripDao().join(passenger, trip);
	}
	
	@POST
	@Path("unjoin/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void unjoinTrip(@PathParam("username") String username, Trip trip) {
		Person passenger = DaoUtils.getInstance().getPersonDao().get(username);
		DaoUtils.getInstance().getTripDao().unjoin(passenger, trip);
	}
	
	@DELETE
	@Path("/{username}/{id}")
	public void deleteTrip(@PathParam("username") String username, @PathParam("id") long id) {
		Trip trip = DaoUtils.getInstance().getTripDao().get(id);
		if (!trip.getCar().getDriver().getUsername().equals(username)) {
			throw new IllegalAccessError("Only the car's driver is allowed to delete a trip");
		}
		
		trip.getCar().getTrips().remove(trip);
		DaoUtils.getInstance().getCarDao().update(trip.getCar());
		
		DaoUtils.getInstance().getTripDao().delete(trip);
	}
}
