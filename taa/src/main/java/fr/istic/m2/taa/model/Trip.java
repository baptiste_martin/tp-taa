package fr.istic.m2.taa.model;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnore;

import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;

@Entity
public class Trip {	
	private long id;
	private Car car;
	private Set<Person> passengers;
	private String start;
	private Set<String> stops;
	private String end;
	private Set<Comment> comments;
	private Date startDate;
	private int nbOfFreeSeats;

	public Trip() {
		car = null;
		passengers = new LinkedHashSet<>();
		start = "";
		stops = new LinkedHashSet<>();
		end = "";
		comments = new LinkedHashSet<>();
		startDate = null;
		nbOfFreeSeats = 0;
	}

	@Id
	@GeneratedValue
	public long getId() {
		return id;
	}
	public Trip setId(long id) {
		this.id = id;
		return this;
	}

	@ManyToOne(targetEntity=Car.class)
	@JoinColumn(name = "car_id")
	public Car getCar() {
		return car;
	}
	public Trip setCar(Car car) {
		this.car = car;
		return this;
	}

	@ManyToMany(targetEntity=Person.class, fetch=FetchType.EAGER, mappedBy="trips")
	public Set<Person> getPassengers() {
		return passengers;
	}
	public Trip setPassengers(Set<Person> passengers) {
		this.passengers = passengers;
		return this;
	}
	
	@Column(name = "startCity")
	public String getStart() {
		return start;
	}
	public Trip setStart(String start) {
		this.start = start;
		return this;
	}
	
	@ElementCollection(fetch = FetchType.EAGER)
	public Set<String> getStops() {
		return stops;
	}
	public Trip setStops(Set<String> stops) {
		this.stops = stops;
		return this;
	}
	
	@Column(name = "endCity")
	public String getEnd() {
		return end;
	}
	public Trip setEnd(String end) {
		this.end = end;
		return this;
	}
	
	@OneToMany(targetEntity=Comment.class, fetch=FetchType.EAGER, mappedBy="trip", cascade={CascadeType.REMOVE}, orphanRemoval = true)
	@JsonIgnore
	public Set<Comment> getComments() {
		return comments;
	}
	public Trip setComments(Set<Comment> comments) {
		this.comments = comments;
		return this;
	}

	public int getNbOfFreeSeats() {
		return nbOfFreeSeats;
	}
	public Trip setNbOfFreeSeats(int nbOfFreeSeats) {
		this.nbOfFreeSeats = nbOfFreeSeats;
		return this;
	}
	
	@Temporal(TemporalType.DATE)
	public Date getStartDate() {
		return startDate;
	}
	public Trip setStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Trip)) {
			throw new ClassCastException();
		}
		Trip castObj = (Trip) obj;
		
		return castObj.id == this.id;
	}
	
	@Override
	public int hashCode(){
		return Long.valueOf(id).hashCode();
	}
}
