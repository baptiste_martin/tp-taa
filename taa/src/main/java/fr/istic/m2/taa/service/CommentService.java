package fr.istic.m2.taa.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;
import fr.istic.m2.taa.service.dao.DaoUtils;

@Path("/comment")
public class CommentService {
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Comment getComment(@PathParam("id") long id) {
		return DaoUtils.getInstance().getCommentDao().get(id);
	}
	
	@GET
	@Path("/withTrip/{trip}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getCommentWithTrip(@PathParam("trip") long tripId, @QueryParam("limit") int limit) {
		Trip trip = DaoUtils.getInstance().getTripDao().get(tripId);
		List<Comment> comments = DaoUtils.getInstance().getCommentDao().getByTrip(trip);
		return comments.subList(0, (limit != 0) ? limit : comments.size());	
	}

	@GET
	@Path("/withPerson/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Comment> getCommentWithPerson(@PathParam("username") String username, @QueryParam("limit") int limit) {
		Person person = DaoUtils.getInstance().getPersonDao().get(username);
		List<Comment> comments = DaoUtils.getInstance().getCommentDao().getByPerson(person);
		return comments.subList(0, (limit != 0) ? limit : comments.size());		
	}

	@PUT
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment setComment(@PathParam("username") String username, Comment comment) {
		Person p = DaoUtils.getInstance().getPersonDao().get(username);
		
		for (Comment c : p.getComments()) {
			if (c.getTrip().equals(comment.getTrip())) {
				throw new IllegalAccessError("You are already comment this trip.");
			}
		}
		
		comment.setPerson(p);
		
		return DaoUtils.getInstance().getCommentDao().add(comment);
	}

	@POST
	@Path("/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment addComment(@PathParam("username") String username, Comment comment) {
		Comment oldComment = DaoUtils.getInstance().getCommentDao().get(comment.getId());
		
		if (oldComment == null || !oldComment.getPerson().getUsername().equals(username)) {
			throw new IllegalAccessError("Your not authorize to set this comment");
		}
		
		comment.setPerson(DaoUtils.getInstance().getPersonDao().get(username));
		
		return DaoUtils.getInstance().getCommentDao().update(comment);
	}

	@DELETE
	@Path("/{username}/{id}")
	public void deleteComment(@PathParam("username") String username, @PathParam("id") long id) {
		Comment comment = DaoUtils.getInstance().getCommentDao().get(id);
		
		if (!comment.getPerson().getUsername().equals(username)) {
			throw new IllegalAccessError("Your not authorize to delete this comment");
		}
		
		DaoUtils.getInstance().getCommentDao().delete(comment);
	}
}