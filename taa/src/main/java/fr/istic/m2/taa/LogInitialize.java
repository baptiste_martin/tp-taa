package fr.istic.m2.taa;

import org.apache.log4j.PropertyConfigurator;

public class LogInitialize {
	static {
		PropertyConfigurator.configure("./src/main/resources/log4j.properties");
	}
}
