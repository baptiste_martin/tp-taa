package fr.istic.m2.taa.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.dao.CarDAO;
import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;

public class CarDAOImpl extends AbstractEntityDAOImpl<Long, Car> implements CarDAO {
	@SuppressWarnings("unchecked")
	public List<Car> getAll() {
		List<Car> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Car c").getResultList();
		entityManager.getTransaction().commit();
		entityManager.close();
		return result;
	}
	
	public Car get(Long id) {
		Car result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		entityManager.getTransaction().begin();
		result = (Car) entityManager.createQuery("select c from Car c where c.id = :id")
				.setParameter("id", id)
				.getSingleResult();
		entityManager.getTransaction().commit();
		entityManager.close();
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Car> getByDriver(Person driver) {
		List<Car> result = null;
		EntityManager entityManager = EntityUtils.getEntityManager();
		entityManager.getTransaction().begin();
		result = entityManager.createQuery("select c from Car c where c.driver = :driver")
				.setParameter("driver", driver)
				.getResultList();
		entityManager.getTransaction().commit();
		entityManager.close();
		return result;
	}
}
