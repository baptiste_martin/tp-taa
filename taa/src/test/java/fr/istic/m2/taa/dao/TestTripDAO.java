package fr.istic.m2.taa.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.TestTaa;
import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;
import fr.istic.m2.taa.service.dao.DaoUtils;

public class TestTripDAO extends TestTaa {
	private final static int MAX_PERSONS_PER_TRIP = 3;
	private final static int MAX_TRIPS = 5;
	private List<Trip> trips;
	private List<Person> persons;
	private Car car;
	private Person driver;
	private Person toto;
	private GregorianCalendar calendar; 
	private Date initialDate;

	@Before
	public void setUp() {
		trips = new ArrayList<Trip>();
		persons = new ArrayList<Person>();
		calendar = new java.util.GregorianCalendar(); 
		
		driver = new Person().setUsername("Driver");
		DaoUtils.getInstance().getPersonDao().add(driver);
		
		toto = new Person().setUsername("toto");
		DaoUtils.getInstance().getPersonDao().add(toto);
		
		car = new Car()
				.setModel("testCar")
				.setNbOfSeats(1)
				.setHasAC(false)
				.setIsSmoker(false)
				.setHasRadio(false);
		DaoUtils.getInstance().getCarDao().add(car);
		
		car.setDriver(driver);
		DaoUtils.getInstance().getCarDao().update(car);
		driver.getCars().add(car);
		DaoUtils.getInstance().getPersonDao().update(driver);

		initialDate = new Date();
		for (int i = 0; i < MAX_TRIPS; i++) {
			calendar.setTime(initialDate);
			calendar.add(Calendar.DATE, i);
			Date d = calendar.getTime();
			
			Trip t = new Trip()
			.setStart("Ville_" + i)
			.setEnd("Ville_" + (i + 1))
			.setStartDate(d)
			.setNbOfFreeSeats(2)
			.setCar(car);
			
			DaoUtils.getInstance().getTripDao().add(t);	// Test add trip
			trips.add(t);
			
			car.getTrips().add(t);
			DaoUtils.getInstance().getCarDao().update(car);
			t.setCar(car);
			
			for (int j = 0; j < MAX_PERSONS_PER_TRIP; j++) {
				Person p = new Person()
				.setUsername("test " + i + " - " + j)
				.setPassword("pass " + i + " - " + j)
				.setPhone("02 00 00 00 00")
				.setMail("test" + j + "@mail.fr");
				
				DaoUtils.getInstance().getPersonDao().add(p);		
				persons.add(p);
				
				p.getTrips().add(t); 
				DaoUtils.getInstance().getPersonDao().update(p);
				t.getPassengers().add(p);
			}
			DaoUtils.getInstance().getTripDao().update(t); // Test update trip
		}
	}

	@After
	public void unsetUp() {
		EntityManager entityManager = EntityUtils.getEntityManager();
		entityManager.getTransaction().begin();
		
		for (Trip t : trips) {
			for (Person p: t.getPassengers()) {
				p.getTrips().remove(t);
				DaoUtils.getInstance().getPersonDao().update(p);
			}
			DaoUtils.getInstance().getTripDao().update(t);
			car.getTrips().remove(t);
			DaoUtils.getInstance().getCarDao().update(car);
			t.setCar(null);
			DaoUtils.getInstance().getTripDao().delete(t);	// Test delete trip
		}
		DaoUtils.getInstance().getCarDao().update(car);
		trips.clear();
		
		for (Person p: persons) {
			p.getCars().clear();			
			DaoUtils.getInstance().getPersonDao().delete(p);
		}
		persons.clear();
		DaoUtils.getInstance().getPersonDao().delete(driver);//the car is deleted together with its driver
		DaoUtils.getInstance().getPersonDao().delete(toto);
		
		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Test
	public void testGetAll() {
		List<Trip> t = DaoUtils.getInstance().getTripDao().getAll();
		assertEquals(MAX_TRIPS, t.size());
		assertEquals(MAX_TRIPS, trips.size());
	}
	
	@Test
	public void testGetTrip(){
		Trip t0 = DaoUtils.getInstance().getTripDao().get(trips.get(0).getId());
		assertEquals(t0, trips.get(0));
		
		Trip t2 = DaoUtils.getInstance().getTripDao().get(trips.get(2).getId());
		assertEquals(t2, trips.get(2));

		Trip tN = DaoUtils.getInstance().getTripDao().get(trips.get(MAX_TRIPS - 1).getId());
		assertEquals(tN, trips.get(MAX_TRIPS - 1));
	}
	
	@Test
	public void testJoin(){
		Trip t = DaoUtils.getInstance().getTripDao().get(trips.get(0).getId());
		assertEquals(2, t.getNbOfFreeSeats());
		DaoUtils.getInstance().getTripDao().join(toto, t);
		assertEquals(1, t.getNbOfFreeSeats());
		assertTrue(t.getPassengers().contains(toto));
		assertTrue(toto.getTrips().contains(t));
		DaoUtils.getInstance().getTripDao().unjoin(toto, t);
	}
	
	@Test
	public void testUnjoin(){
		Trip t = DaoUtils.getInstance().getTripDao().get(trips.get(0).getId());
		DaoUtils.getInstance().getTripDao().join(toto, t);
		assertEquals(1, t.getNbOfFreeSeats());
		DaoUtils.getInstance().getTripDao().unjoin(toto, t);
		assertEquals(2, t.getNbOfFreeSeats());
		assertFalse(t.getPassengers().contains(toto));
		assertFalse(toto.getTrips().contains(t));
	}
	
	@Test
	public void testGetByDateAndPlaces(){
		calendar.setTime(initialDate);
		calendar.add(Calendar.DATE, 1);
		Date dayAfter = calendar.getTime();
		//Get trip from initial date from Ville_0 to Ville_1
		List<Trip> trips = DaoUtils.getInstance().getTripDao().getByDateAndPlaces("Ville_0", "Ville_1", initialDate, initialDate);
		assertEquals(1, trips.size());
		//Get trip between initial date and the day after from Ville_0 to Ville_1
		trips = DaoUtils.getInstance().getTripDao().getByDateAndPlaces("Ville_0", "Ville_1", initialDate, dayAfter);
		assertEquals(1, trips.size());
		//There's no trip the day after from Ville_0 to Ville_1
		trips = DaoUtils.getInstance().getTripDao().getByDateAndPlaces("Ville_0", "Ville_1", dayAfter, dayAfter);
		assertEquals(0, trips.size());
		//There's no trip the initial date from Ville_1 to Ville_2
		trips = DaoUtils.getInstance().getTripDao().getByDateAndPlaces("Ville_1", "Ville_2", initialDate, initialDate);
		assertEquals(0, trips.size());
		//Get trip the day after from Ville_1 to Ville_2
		trips = DaoUtils.getInstance().getTripDao().getByDateAndPlaces("Ville_1", "Ville_2", dayAfter, dayAfter);
		assertEquals(1, trips.size());
	}
}
