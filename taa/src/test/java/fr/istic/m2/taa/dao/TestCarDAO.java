package fr.istic.m2.taa.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.istic.m2.taa.TestTaa;
import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.service.dao.DaoUtils;

public class TestCarDAO extends TestTaa {
	public static Logger log = Logger.getLogger(TestCarDAO.class);
	private final static int MAX_CARS = 5;
	private List<Car> cars;
	private Person person;

	@Before
	public void setUp() {
		cars = new ArrayList<Car>();
		
		person = new Person().setUsername("test");
		DaoUtils.getInstance().getPersonDao().add(person);
		
		for (int i = 0; i < MAX_CARS; i++) {
			Car c = new Car()
				.setModel("test" + i)
				.setNbOfSeats(i)
				.setHasAC(i%4 == 0)
				.setIsSmoker(i%3 == 0)
				.setHasRadio(i%2 == 0);
			DaoUtils.getInstance().getCarDao().add(c);	// Test add car
			cars.add(c);
		}
	}
	
	@After
	public void unsetUp() {
		for (Car c : cars) {
			DaoUtils.getInstance().getCarDao().delete(c);	// Test delete car
		}
		cars.clear();
		
		DaoUtils.getInstance().getPersonDao().delete(person);
	}
	
	@Test
	public void testGetAll() {
		List<Car> c = DaoUtils.getInstance().getCarDao().getAll();
		assertEquals(MAX_CARS, c.size());
	}
	
	@Test
	public void testGet() {
		Car c0 = DaoUtils.getInstance().getCarDao().get(cars.get(0).getId());
		assertEquals(c0, cars.get(0));
		
		Car c2 = DaoUtils.getInstance().getCarDao().get(cars.get(2).getId());
		assertEquals(c2, cars.get(2));

		Car cN = DaoUtils.getInstance().getCarDao().get(cars.get(MAX_CARS - 1).getId());
		assertEquals(cN, cars.get(MAX_CARS - 1));
	}

	@Test
	public void testGetByDriver() {
		List<Car> c = DaoUtils.getInstance().getCarDao().getByDriver(person);
		assertTrue(c.size() == 0);

		Car car = cars.get(0).setDriver(person);
		DaoUtils.getInstance().getCarDao().update(car);	// Test update car

		List<Car> c1 = DaoUtils.getInstance().getCarDao().getByDriver(person);
		assertNotNull(c1);
		assertTrue(c1.get(0).getDriver().equals(person));
		assertTrue(car.equals(c1.get(0)));
	}
}
