package fr.istic.m2.taa;

//import java.util.Collection;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;

//import static org.junit.Assert.*;

public abstract class TestTaa {	
	private static Logger log = Logger.getLogger(TestTaa.class);
	
	@BeforeClass
	public static void initBDD() {
		PropertyConfigurator.configure("./src/main/resources/log4jTest.properties");

		log.info("Set mode to EntityUtils to test");
		
		EntityUtils.setMode("test");	// Initialize entity utils test
	}
	
	/*protected static <E> void assertContains(Collection<E> col, E e) {
		for (E eCol : col) {
			if (eCol.equals(e)) {
				return;
			}
		}
		fail(null);
	}*/
}
