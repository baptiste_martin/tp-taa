package fr.istic.m2.taa.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import fr.istic.m2.taa.EntityUtils;
import fr.istic.m2.taa.TestTaa;
import fr.istic.m2.taa.model.Car;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.service.dao.DaoUtils;

public class TestPersonDAO extends TestTaa {
	private final static int MAX_PERSONS = 40;
	private final static int MAX_CARS = 5;
	private List<Person> persons;

	@Before
	public void setUp() {
		persons = new ArrayList<Person>();

		for (int i = 0; i < MAX_PERSONS; i++) {
			Person p = new Person()
				.setUsername("test" + i)
				.setPassword("pass" + i)
				.setPhone("02 00 00 00 00")
				.setMail("test" + i + "@mail.fr");
			DaoUtils.getInstance().getPersonDao().add(p);	// Test add person
			persons.add(p);
			
			for (int j = 0; j < MAX_CARS; j++) {
				Car c = new Car()
					.setModel("test" + i + j);
				DaoUtils.getInstance().getCarDao().add(c);
				c.setDriver(p);	
				DaoUtils.getInstance().getCarDao().update(c);
				
				p.getCars().add(c);
				DaoUtils.getInstance().getPersonDao().update(p);	// Test update person
			}
		}
	}

	@After
	public void unsetUp() {
		EntityManager entityManager = EntityUtils.getEntityManager();
		entityManager.getTransaction().begin();

		for (Person p : persons) {
			DaoUtils.getInstance().getPersonDao().delete(p);	// Test delete person
		}
		persons.clear();

		entityManager.getTransaction().commit();
		entityManager.close();
	}

	@Test
	public void testGetAll() {
		List<Person> p = DaoUtils.getInstance().getPersonDao().getAll();
		assertTrue(p.size() == MAX_PERSONS);
	}

	@Test
	public void testGet() {
		Person p = DaoUtils.getInstance().getPersonDao().get("test%");
		assertNull(p);

		Person p1 = DaoUtils.getInstance().getPersonDao().get("test0");
		assertNotNull(p1);
		assertTrue(p1.getUsername().equals("test0"));

		Person p2 = DaoUtils.getInstance().getPersonDao().get("test5");
		assertNotNull(p2);
		assertTrue(p2.getUsername().equals("test5"));

		Person p3 = DaoUtils.getInstance().getPersonDao().get("test" + (MAX_PERSONS - 1));
		assertNotNull(p3);
		assertTrue(p3.getUsername().equals("test" + (MAX_PERSONS - 1)));
	}
	
	@Test
	public void testGetCar() {
		Person p1 = DaoUtils.getInstance().getPersonDao().get("test0");
		assertNotNull(p1);
		
		assertEquals(persons.get(0).getCars().size(), p1.getCars().size());
		
		for (Car c : p1.getCars()) {
			assertTrue(persons.get(0).getCars().contains(c));
			assertTrue(c.getDriver().equals(p1));
		}
	}
}
