package fr.istic.m2.taa.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import fr.istic.m2.taa.TestTaa;
import fr.istic.m2.taa.model.Comment;
import fr.istic.m2.taa.model.Person;
import fr.istic.m2.taa.model.Trip;
import fr.istic.m2.taa.service.dao.DaoUtils;

public class TestCommentDAO extends TestTaa {
	private final static int MAX_COMMENTS = 10;
	private List<Comment> comments;
	private List<Person> persons;
	private Trip trip;

	@Before
	public void setUp() {
		comments = new ArrayList<Comment>();
		persons = new ArrayList<Person>();
		
		trip = new Trip()
			.setStart("Rennes")
			.setEnd("Brest")
			.setStartDate(new Date());
		DaoUtils.getInstance().getTripDao().add(trip);

		for (int i = 0; i < MAX_COMMENTS; i++) {
			Person p = new Person()
				.setUsername("test" + i);
			DaoUtils.getInstance().getPersonDao().add(p);
			persons.add(p);
		
			Comment c = new Comment()
				.setComments("comment" + i)
				.setRating((i % 6));
			
			DaoUtils.getInstance().getCommentDao().add(c);	// Test add comment
			comments.add(c);

			c.setPerson(p)
				.setTrip(trip);
			
			DaoUtils.getInstance().getCommentDao().update(c);	// Test update comment
			
			p.getComments().add(c);
			DaoUtils.getInstance().getPersonDao().update(p);
			
			trip.getComments().add(c);
		}
		DaoUtils.getInstance().getTripDao().update(trip);
	}

	@After
	public void unsetUp() {
		for (Comment c : comments) {
			DaoUtils.getInstance().getCommentDao().delete(c);	// Test delete comment
		}
		comments.clear();
		
		for (Person p : persons) {
			p.getComments().clear();
			DaoUtils.getInstance().getPersonDao().delete(p);
		}
		persons.clear();
		trip.getComments().clear();
		trip.getPassengers().clear();
		DaoUtils.getInstance().getTripDao().delete(trip);
	}

	@Test
	public void testGetAll() {
		List<Comment> p = DaoUtils.getInstance().getCommentDao().getAll();
		assertTrue(p.size() == MAX_COMMENTS);
	}

	@Test
	public void testGetByPerson() {
		assertTrue(DaoUtils.getInstance().getCommentDao().getByPerson(new Person()).isEmpty());
		
		List<Comment> commentsP = DaoUtils.getInstance().getCommentDao().getByPerson(persons.get(0));
		assertFalse(commentsP.isEmpty());
		assertTrue(commentsP.get(0).getPerson().getUsername().equals("test0"));
		commentsP.clear();
		
		commentsP = DaoUtils.getInstance().getCommentDao().getByPerson(persons.get(1));
		assertFalse(commentsP.isEmpty());
		assertTrue(commentsP.get(0).getPerson().getUsername().equals("test1"));
		commentsP.clear();
		
		commentsP = DaoUtils.getInstance().getCommentDao().getByPerson(persons.get(5));
		assertFalse(commentsP.isEmpty());
		assertTrue(commentsP.get(0).getPerson().getUsername().equals("test5"));
		commentsP.clear();
		
		commentsP = DaoUtils.getInstance().getCommentDao().getByPerson(persons.get(MAX_COMMENTS - 1));
		assertFalse(commentsP.isEmpty());
		assertTrue(commentsP.get(0).getPerson().getUsername()
				.equals("test" + (MAX_COMMENTS - 1)));
		commentsP.clear();
	}

	@Test
	public void testGetByTrip() {
		assertTrue(DaoUtils.getInstance().getCommentDao().getByTrip(new Trip().setId(555)).isEmpty());
		
		List<Comment> commentsP = DaoUtils.getInstance().getCommentDao().getByTrip(trip);
		assertFalse(commentsP.isEmpty());
		assertEquals(commentsP.get(0).getTrip().getId(), trip.getId());
		commentsP.clear();
	}

	@Test
	public void testGetByRating() {
		assertTrue(DaoUtils.getInstance().getCommentDao().getByRating(6).isEmpty());

		List<Comment> commentsP = DaoUtils.getInstance().getCommentDao().getByRating(5);
		assertFalse(commentsP.isEmpty());
		assertTrue(commentsP.get(0).getPerson().getUsername().equals("test5"));
		commentsP.clear();
	}
}
